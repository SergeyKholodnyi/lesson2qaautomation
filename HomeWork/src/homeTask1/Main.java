package homeTask1;

public class Main {

    public static void main(String[] args) {
        Developer javaDeveloper = new JavaDeveloper("java 8");
        Developer jsDeveloper = new JavaScriptDeveloper("angular");
        System.out.println(DeveloperFunction.createProgramHelloWorld(javaDeveloper));
    }
}
