package homeTask1;

public abstract class Developer {

    protected String language;

    protected Developer(String language) {
        this.language = language;
    }

    abstract String createProgramHelloWorld();

}
