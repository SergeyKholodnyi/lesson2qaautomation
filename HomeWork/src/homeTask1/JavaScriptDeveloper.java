package homeTask1;

public class JavaScriptDeveloper extends Developer {

    private String framework;

    public JavaScriptDeveloper(String language) {
        super(language);
    }

    @Override
    String createProgramHelloWorld() {
       return "<script>\n" +
               "    alert( 'Привет, Мир!' );\n" +
               "  </script>"+language;
    }


    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }
}
