package homeTask1;

public class JavaDeveloper extends Developer {

    private String javaVersion;

    public JavaDeveloper(String language) {
        super(language);
    }

    @Override
    String createProgramHelloWorld() {
        return "System.out.prinln(\"Hello World!\")"+language;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public void setJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
    }

    public static void main(String[] args) {
        System.out.println("");
    }
}
